package org.example.ex1;


import org.example.database.DataBaseConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Ex1 {
    public static void main(String[] args) {
        // adaugati 2 filme si 3 actori intr-o singura tranzactie
        // - creare SessionFactory
        // - creare de Tabele
        // - inserare Date


        Session session = DataBaseConfig.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Movie movie = new Movie();

        movie.setId(12);
        movie.setName("WrongTurn");
        movie.setGen(Genre.ACTION);
        session.persist(movie);


        movie.setId(13);
        movie.setName("Scarry Movie");
        movie.setGen(Genre.THRILLER);
        session.persist(movie);


        Actor actor = new Actor();

        transaction.commit();
        session.close();
    }
}
