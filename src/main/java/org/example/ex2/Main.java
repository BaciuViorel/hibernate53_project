package org.example.ex2;

import org.example.database.DataBaseConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        // 1 metoda de setare
        Students students = new Students(2,"Alin",1995);
        // 2 metoda de setare
        students.setId(1);
        students.setName("Antonio");
        students.setBirthYear(1994);
        session.persist(students);
        session.close();
    }
}