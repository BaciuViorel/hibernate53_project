package org.example.ex1;

import jakarta.persistence.*;

import java.util.Date;

// id
// numele
// relase date
// incasari
// genul
//
@Entity

public class Movie {
    @Id
    private Integer Id;
    private String name;
    @Column(name= "release date")
    private Date relaseDate;
    private Double revenue;
    @Column(name= "imbd_score")
    private Integer imbdScore;

    // de verificat aceasta addnoatation
    @Enumerated(value = EnumType.STRING)
    private Genre gen;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRelaseDate() {
        return relaseDate;
    }

    public void setRelaseDate(Date relaseDate) {
        this.relaseDate = relaseDate;
    }

    public Double getRevenue() {
        return revenue;
    }

    public void setRevenue(Double revenue) {
        this.revenue = revenue;
    }

    public Integer getImbdScore() {
        return imbdScore;
    }

    public void setImbdScore(Integer imbdScore) {
        this.imbdScore = imbdScore;
    }

    public Genre getGen() {
        return gen;
    }

    public void setGen(Genre gen) {
        this.gen = gen;
    }
}
