package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.relations.entity.Child;
import org.example.relations.entity.Food;
import org.example.relations.entity.Mother;
import org.example.relations.entity.Toy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class OneToOneMain {
    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();


        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Food f1 = new Food(1,"milk", true);
        session.persist(f1);

        Toy t1 = new Toy(1,"Pokemon");
        // session.persist(toy1) , nu trebuie facut din cauza ca avem cascade

        Child c1 = new Child(2,"Johnny",f1, t1);
        session.persist(c1);


        transaction.commit();
        System.out.println("Child was saved ");

        Transaction t2 = session.beginTransaction();
        session.remove(c1);
        t2.commit();
        System.out.println("Child was removed");

        session.close();
    }
}
