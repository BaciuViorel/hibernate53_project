package org.example.relations.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "owners")
public class Owner {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer Id;
    private String name;

    @OneToMany(cascade = {CascadeType.PERSIST})
    @JoinColumn(name = " owner_id ")
    List<Animal> pets;

    public Owner(Integer Id, String name, List<Animal> pets) {
        this.Id = Id;
        this.name = name;
        this.pets = pets;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Animal> getPets() {
        return pets;
    }

    public void setPets(List<Animal> pets) {
        this.pets = pets;
    }

    public void addPet(Animal newPet) {
        this.pets.add(newPet);
    }

    public void removePet(Animal pet) {
        this.pets.remove(pet);
    }
}
