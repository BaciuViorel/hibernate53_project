package org.example.enity;

import org.example.database.DataBaseConfig;
import org.example.enity.Car;
import org.example.enity.Driver;
import org.example.enity.Truck;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

// 1.creare tabele
// 2. inserare date
public class Main {
    public static void main(String[] args) {

//        Configuration hibernateConfiguration = new Configuration();
//        hibernateConfiguration.configure("hibernate.config.xml")
        // este un obiect pe care il vom folosi ca sa ne connectam la baza de date
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();


        // ne folosim aici de obiectul SessionFactory de mai sus, aici session ia valoarea sessionFactory
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Car c = new Car();
        c.setId(11);
        c.setName("Test car");
        session.persist(c); // un fel de INSERT  , trebuie facut pentru fiecare obiect pe care il salvam


        Driver d = new Driver();
        d.setId(12);
        d.setName("Test Driver");
        d.setMaxTravelDistance(1200);
        d.setEmail("Baciu_viorel94@yahoo.com");
        d.setSecurityKey("234");
        session.persist(d);


        Truck t = new Truck();
        t.setId(15);
        t.setName("Test Truck");
        session.persist(t);


        transaction.commit();
        session.close();
    }
}
