package org.example.relations;

import org.example.database.DataBaseConfig;
import org.example.ex1.Genre;
import org.example.relations.entity.Hobby;
import org.example.relations.entity.Job;
import org.example.relations.entity.Mother;
import org.example.relations.entity.TvShow;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;


public class OneToManyMain {
    public static void main(String[] args) {
        SessionFactory sessionFactory = DataBaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction t1 = session.beginTransaction();

        Hobby hobby1 = new Hobby(null, "Tenis", "amateur");
        Hobby hobby2 = new Hobby(null,"Fotbal", "profesional");
        session.persist(hobby1);
        session.persist(hobby2);

        TvShow tvShow1 = new TvShow(null,"Manifest", Genre.COMEDY);
        TvShow tvShow2 = new TvShow(null,"Dosarele X", Genre.ACTION);
        // Nu vrem sa le persistam deoarece operatia de Persist este cascadata din Mother

        Mother m1 = new Mother(null,"Ioana", Job.LAWYER, List.of(hobby1,hobby2), List.of(tvShow1,tvShow2));
        session.persist(m1);


        t1.commit();
        session.close();
    }
}
