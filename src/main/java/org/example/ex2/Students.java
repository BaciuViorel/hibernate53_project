package org.example.ex2;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Table (name = "students")
@Entity
public class Students {
    @Id
    private Integer Id;
    @Column(nullable = false)
    private String name;
    @Column(name = "birth_year")
    private Integer birthYear;



    public Students() {
    }

    public Students(Integer id, String name, Integer birthYear) {
        Id = id;
        this.name = name;
        this.birthYear = birthYear;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(Integer birthYear) {
        this.birthYear = birthYear;
    }
}
